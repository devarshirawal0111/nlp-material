#get target and source vocabulary files (*.vcb) as well as a sentence pair files (*.snt).
../scripts/plain2snt.out ../data/train.en ../data/train.fr

#generating coocrence file using .vcb and .snt files. Use source_target snt file
../scripts/snt2cooc.out ../data/train.en.vcb ../data/train.fr.vcb ../data/train.en_train.fr.snt > ../data/cooc.cooc

#Generating Word Alignments 
../scripts/GIZA++ -s ../data/train.en.vcb -t ../data/train.fr.vcb -c ../data/train.en_train.fr.snt -CoocurrenceFile ../data/cooc.cooc 

